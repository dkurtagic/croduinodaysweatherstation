//
//  WeatherRecord.h
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 23/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface WeatherRecord : NSObject
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSNumber *hum;
@property (nonatomic, strong) NSNumber *temp;

- (id)initWithDictionary:(NSDictionary*)dict;
@end
