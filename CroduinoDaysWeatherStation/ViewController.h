//
//  ViewController.h
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 23/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

