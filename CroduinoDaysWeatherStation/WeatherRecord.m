//
//  WeatherRecord.m
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 23/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import "WeatherRecord.h"

@implementation WeatherRecord
- (id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init])
    {
        NSString *dateString = [dict objectForKey:@"datetime"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        _date = [formatter dateFromString:dateString];
    
        NSString *humString = [dict objectForKey:@"hum"];
        _hum = [NSNumber numberWithFloat:humString.floatValue];
        
        NSString *tempString = [dict objectForKey:@"temp"];
        _temp = [NSNumber numberWithFloat:tempString.floatValue];
    }
    return self;
}
@end
