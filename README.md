# README #

This repository contains code of iOS application with responding PHP web script as a example of connecting Croduino (Arduino compatible) prototyping board to internet database. Data has been read from the sensors, and sent by simple GET HTTP request to the API.

This example is made for E-Radionica Croduino Days workshops.

Version 1.0. is initially commited.

### Set up ###
* Using Cocoa Pods in XCode Project (please run XCode workspace)
* API works with MySql database, base, table, and attributes can be read from the .php files and recreated on your server


### Contributions ###

Contributions to initial version belongs to Dino Kurtagic (dkurtagic@gmail.com), [LeecheSystems Team](http://www.lychee.systems).

Code is free for use, modification, and further improvement, pull and push.